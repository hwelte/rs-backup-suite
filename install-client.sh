#!/bin/sh
##
# Copyright (C) 2013-2016 Janek Bevendorff
# Website: http://www.refining-linux.org/
# 
# Install script for installing server and client script files
# 
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
##

if [[ "$1" != "all" ]] && [[ "$1" != "client" ]] && [[ "$1" != "server" ]]; then
	./server/usr/bin/rs-version headline "rs-backup-suite installer"
	./server/usr/bin/rs-version copyright
	echo
	echo "Usage: $(basename $0) [all|server|client]"
	exit
fi

if [ $(id -u) -ne 0 ] && [[ "$(uname -o)" != "Cygwin" ]]; then
	echo "ERROR: This script must be run as root."
	exit 1
fi


###############################################################################
# Global variables
###############################################################################
DISTRIBUTION="$(./server/usr/bin/rs-detect-distribution)"
COMPONENT="$1"
MODE="install"
if [[ "$(basename $0)" == "uninstall.sh" ]]; then
	MODE="uninstall"
fi


###############################################################################
# Command aliases
###############################################################################
CP="cp -vr --preserve=mode,timestamps,links"
RM="rm -Rvf"
MKDIR="mkdir -pv"


	# Client component
		echo "Installing client component..."

		$CP ./client/usr/bin/* /usr/bin/
		$CP ./server/usr/bin/rs-version /usr/bin/

		if [ -d /etc/systemd/system ]; then
			echo 'Detected systemd. Run `systemctl enable rs-backup-run.timer` to enable daily backups.'
			$CP ./client/etc/systemd/system/* /etc/systemd/system
		fi

		# Do not overwrite existing config
		if [ ! -e /etc/rs-backup/client-config ]; then
			$CP ./client/etc/rs-backup /etc/
		elif ! $(cmp --silent ./client/etc/rs-backup/client-config /etc/rs-backup/client-config); then
			$CP ./client/etc/rs-backup/client-config /etc/rs-backup/client-config.new
		fi

		echo "Done."
